﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using Screen;

namespace GameProject
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public GameplayScreen mGameplayScreen;
        public MainMenu mMainMenu;
        public GameOver mgameoverscreen;
        public screen mCurrentScreen;
        public Cutscene mCutscene;
        public HowState mhowState;
        private int hp = 0;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.ApplyChanges();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mGameplayScreen = new GameplayScreen(this, new EventHandler(GameplayScreenEvent));
            mMainMenu = new MainMenu(this, new EventHandler(GameplayScreenEvent));
            mgameoverscreen = new GameOver(this, new EventHandler(GameplayScreenEvent));
            mCutscene = new Cutscene(this, new EventHandler(GameplayScreenEvent));
            mhowState = new HowState(this, new EventHandler(GameplayScreenEvent));
            mCurrentScreen = mMainMenu;
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            mCurrentScreen.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            mCurrentScreen.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public void GameplayScreenEvent(object obj, EventArgs e)
        {
            mCurrentScreen = (screen)obj;
        }
    }
}