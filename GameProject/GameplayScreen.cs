﻿using System;
using System.Collections.Generic;
using System.IO;
using Animating;
using Screen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace GameProject
{
    public class GameplayScreen : screen
    {
        Game1 game;

        //Text
        SpriteFont pressedPower;
        SpriteFont mineralScore;

        //Song
        //Song powerSound;
        Song getitemSound;
        Song getitemSound2;
        Song dropitemSound;
        Song lostpowerSound;

        List<SoundEffect> effects;

        Texture2D Minerals;
        Vector2[] MineralsPosition;
        Vector2[] MineralsPosition2;

        List<Vector2> blockPosition = new List<Vector2>();
        List<Vector2> blockPosition2 = new List<Vector2>();
        List<Vector2> blockPosition3 = new List<Vector2>();
        List<Vector2> blockPosition4 = new List<Vector2>();
        List<Vector2> blockPosition5 = new List<Vector2>();
        List<Vector2> blockPosition6 = new List<Vector2>();
        List<Vector2> blockPosition7 = new List<Vector2>();

        int max_minerals = 20;
        int[] MI;
        bool ManHit = false;

        Texture2D Storage_BG;
        Texture2D Charge_BG;
        Texture2D Sci_BG;
        Texture2D Device_BG;

        Texture2D BAR;
        Texture2D BG;
        Texture2D Waterroom;
        Texture2D Nature;
        Texture2D Bag;
        Texture2D Box_1;
        Texture2D Sky;
        Texture2D Sand;

        Texture2D empty;

        Vector2 scroll_factor = new Vector2(1.0f, 1);
        Vector2 cameraPos = Vector2.Zero;
        Vector2 BGPos = Vector2.Zero;

        private AnimatedTexture Player;
        private AnimatedTexture Player_charge;

        private const float Rotation = 0;
        private const float Scale = 1.0f;
        private const float Depth = 0.5f;
        private Vector2 xPos = new Vector2(0, 0);
        private const int Frames = 6;
        private const int FramesPerSec = 10;
        private const int FramesRow = 8;
        int direction = 1;
        int direction2 = 1;
        private float elapsed;

        private Vector2 xPos2 = new Vector2(780, 150);
        private const int Frames2 = 6;
        private const int FramesPerSec2 = 10;
        private const int FramesRow2 = 3;

        public float score = 0;
        public float playerhp = 250f;
        public float playerenergy = 250f;

        Vector2 size = new Vector2(55, 80);

        float nightFactor = 0f;
        int nightDirection = 1;

        int[] pocket = new int[4];

        Rectangle roomRectangle = new Rectangle(215, 180, 85, 120);
        Rectangle roomRectangle2 = new Rectangle(760, 180, 100, 120);
        Rectangle roomRectangle3 = new Rectangle(175, 305, 420, 180);
        Rectangle roomRectangle4 = new Rectangle(675, 305, 420, 180);
        Rectangle roomRectangle5 = new Rectangle(0, 120, 185, 2080);
        Rectangle roomRectangle6 = new Rectangle(1085, 120, 185, 2080);
        Rectangle roomRectangle7 = new Rectangle(0, 0, 1280, 34);
        Rectangle roomRectangle8 = new Rectangle(0, 118, 540, 90);
        Rectangle roomRectangle9 = new Rectangle(660, 0, 620, 205);

        Random r = new Random();

        bool recharge = false;

        bool drill = false;

        public GameplayScreen(Game1 game, EventHandler theScreenEvent)
            : base(theScreenEvent)
        {

            /*
            StreamWriter sw = new StreamWriter("highscore.txt");
            sw.Write(1000);
            sw.Close();
            */
            effects = new List<SoundEffect>();
            try
            {

                StreamReader sr = new StreamReader("highscore.txt");
                int value = int.Parse(sr.ReadLine());
                Console.WriteLine(value);
                sr.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("error");
                StreamWriter sw = new StreamWriter("highscore.txt");
                sw.Write(0);
                sw.Close();
            }

            //Load texture

            Player = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            Player_charge = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);

            BG = game.Content.Load<Texture2D>("ground");
            Sky = game.Content.Load<Texture2D>("Background");
            Waterroom = game.Content.Load<Texture2D>("waterroom");
            Nature = game.Content.Load<Texture2D>("nature");
            Bag = game.Content.Load<Texture2D>("Backpack");
            Box_1 = game.Content.Load<Texture2D>("balloon");
            BAR = game.Content.Load<Texture2D>("BLOOD BARS");
            Minerals = game.Content.Load<Texture2D>("Minerals");
            Sand = game.Content.Load<Texture2D>("Sand");
            Storage_BG = game.Content.Load<Texture2D>("Storage_BG");
            Charge_BG = game.Content.Load<Texture2D>("Charge_BG");
            Sci_BG = game.Content.Load<Texture2D>("Sci_BG");
            Device_BG = game.Content.Load<Texture2D>("Device_BG");
            Player_charge.Load(game.Content, "Charge_ani", Frames2, FramesRow2, FramesPerSec2);
            empty = game.Content.Load<Texture2D>("BG_Menu");
            Player.Load(game.Content, "Player", Frames, FramesRow, FramesPerSec);
            //text Load
            mineralScore = game.Content.Load<SpriteFont>("Start");
            pressedPower = game.Content.Load<SpriteFont>("Start");
            //Effect
            effects.Add(game.Content.Load<SoundEffect>("POWER2"));//0
            effects.Add(game.Content.Load<SoundEffect>("GET_ITEM2"));//1
            effects.Add(game.Content.Load<SoundEffect>("EATING2"));//2
            effects.Add(game.Content.Load<SoundEffect>("LOST_POWER2"));//3
            reset();

            this.game = game;
        }

        void MediaPlayer_MediaStateChanged(object sender, System.EventArgs e)
        {
            // 0.0f is silent, 1.0f is full volume
            MediaPlayer.Volume -= 0.1f;
            //MediaPlayer.Play(powerSound);
          
        }

        public void reset()
        {
            for (int i = 0; i < 18; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    blockPosition.Add(new Vector2(i * 50 + 190, j * 50 + 485));
                    //blockPosition.Add(new Vector2(i * 50 + 190, j * 50 + 720));
                }
            }

            for (int i = 0; i < 18; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    blockPosition2.Add(new Vector2(i * 50 + 190, j * 50 + 735));
                }
            }

            for (int i = 0; i < 18; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    blockPosition3.Add(new Vector2(i * 50 + 190, j * 50 + 985));
                }
            }

            for (int i = 0; i < 18; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    blockPosition4.Add(new Vector2(i * 50 + 190, j * 50 + 1235));
                }
            }

            for (int i = 0; i < 18; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    blockPosition5.Add(new Vector2(i * 50 + 190, j * 50 + 1485));
                }
            }

            for (int i = 0; i < 18; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    blockPosition6.Add(new Vector2(i * 50 + 190, j * 50 + 1735));
                }
            }

            for (int i = 0; i < 18; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    blockPosition7.Add(new Vector2(i * 50 + 190, j * 50 + 1985));
                }
            }

            Random r = new Random();
            MineralsPosition = new Vector2[max_minerals];
            MineralsPosition2 = new Vector2[max_minerals];
            MI = new int[max_minerals];

            xPos = new Vector2(30, 35);
            Vector2 cameraPos = Vector2.Zero;

            for (int i = 0; i < MineralsPosition.Length; i++)
            {
                MineralsPosition[i] = new Vector2(r.Next(180, 1000), r.Next(720, 1440));
                MI[i] = r.Next(0, 4);
            }
            for (int i = 0; i < MineralsPosition2.Length; i++)
            {
                MineralsPosition2[i] = new Vector2(r.Next(180, 1000), r.Next(720, 1440));
                MI[i] = r.Next(0, 4);
            }

            pocket[0] = -1;
            pocket[1] = -1;
            pocket[2] = -1;
            pocket[3] = -1;

            playerhp = 250;
            playerenergy = 250;
            score = 0;

            nightFactor = 0f;
        }

        public override void Update(GameTime theTime)
        {
            elapsed = (float)theTime.ElapsedGameTime.TotalSeconds;

            //before walk
            Vector2 oldPos = xPos;

            processinput();
            //after walk

            if(playerenergy < 0)
            {
                playerhp -= 1;
            }

            if(playerenergy > 250)
            {
                playerenergy = 250;
            }
            if(playerhp > 250)
            {
                playerhp = 250;
            }

            if (nightFactor < 0.5f)
            {
                playerhp -= (2 * elapsed);

            }
            else
            {

                playerhp -= (1 * elapsed);
            }



            if (playerhp < 0)
            {
                Console.WriteLine("dead");
                ScreenEvent.Invoke(game.mgameoverscreen, new EventArgs());
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //System.Console.WriteLine("player energy = " + playerenergy + "playerhp = " + playerhp);

            Player.UpdateFrame(elapsed);

            nightFactor += 0.1f * elapsed * nightDirection;

            if (nightFactor > 1 || nightFactor < 0)
            {
                nightDirection *= -1;
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            System.Console.WriteLine("xpos.x = " + xPos.X + "xpos.y = " + xPos.Y);


            Rectangle personRectangle = new Rectangle((int)xPos.X, (int)xPos.Y, (int)size.X, (int)size.Y);

            /*roomRectangle2.X - cameraPos.X;
            roomRectangle2.Y - cameraPos.Y;
            */
            if (personRectangle.Intersects(roomRectangle3))
            {
                xPos = oldPos;
            }
            if (personRectangle.Intersects(roomRectangle4))
            {
                xPos = oldPos;
            }
            if (personRectangle.Intersects(roomRectangle5))
            {
                xPos = oldPos;
            }
            if (personRectangle.Intersects(roomRectangle6))
            {
                xPos = oldPos;
            }
            if (personRectangle.Intersects(roomRectangle7))
            {
                xPos = oldPos;
            }
            if (personRectangle.Intersects(roomRectangle8))
            {
                xPos = oldPos;
            }
            if (personRectangle.Intersects(roomRectangle9))
            {
                xPos = oldPos;
            }

            for (int i = 0; i < blockPosition.Count; i++)
            {
                for(int j = 0; j < blockPosition.Count; j++)
                {
                    Rectangle blockRect = new Rectangle((int)blockPosition[i].X, (int)blockPosition[i].Y, 50, 50);
                    if (personRectangle.Intersects(blockRect))
                    {
                        xPos = oldPos;
                    }
                }
            }

            for (int i = 0; i < blockPosition2.Count; i++)
            {
                for (int j = 0; j < blockPosition2.Count; j++)
                {
                    Rectangle blockRect2 = new Rectangle((int)blockPosition2[i].X, (int)blockPosition2[i].Y, 50, 50);
                    if (personRectangle.Intersects(blockRect2))
                    {
                        xPos = oldPos;
                    }
                }
            }

            for (int i = 0; i < blockPosition3.Count; i++)
            {
                for (int j = 0; j < blockPosition3.Count; j++)
                {
                    Rectangle blockRect3 = new Rectangle((int)blockPosition3[i].X, (int)blockPosition3[i].Y, 50, 50);
                    if (personRectangle.Intersects(blockRect3))
                    {
                        xPos = oldPos;
                    }
                }
            }

            for (int i = 0; i < blockPosition4.Count; i++)
            {
                for (int j = 0; j < blockPosition4.Count; j++)
                {
                    Rectangle blockRect4 = new Rectangle((int)blockPosition4[i].X, (int)blockPosition4[i].Y, 50, 50);
                    if (personRectangle.Intersects(blockRect4))
                    {
                        xPos = oldPos;
                    }
                }
            }

            for (int i = 0; i < blockPosition5.Count; i++)
            {
                for (int j = 0; j < blockPosition5.Count; j++)
                {
                    Rectangle blockRect5 = new Rectangle((int)blockPosition5[i].X, (int)blockPosition5[i].Y, 50, 50);
                    if (personRectangle.Intersects(blockRect5))
                    {
                        xPos = oldPos;
                    }
                }
            }

            for (int i = 0; i < blockPosition6.Count; i++)
            {
                for (int j = 0; j < blockPosition6.Count; j++)
                {
                    Rectangle blockRect6 = new Rectangle((int)blockPosition6[i].X, (int)blockPosition6[i].Y, 50, 50);
                    if (personRectangle.Intersects(blockRect6))
                    {
                        xPos = oldPos;
                    }
                }
            }

            for (int i = 0; i < blockPosition7.Count; i++)
            {
                for (int j = 0; j < blockPosition7.Count; j++)
                {
                    Rectangle blockRect7 = new Rectangle((int)blockPosition7[i].X, (int)blockPosition7[i].Y, 50, 50);
                    if (personRectangle.Intersects(blockRect7))
                    {
                        xPos = oldPos;
                    }
                }
            }



            // create rectangle
            for (int i = 0; i < max_minerals; i++)
            {
                Rectangle MineralsRectangle = new Rectangle((int)MineralsPosition[i].X, (int)MineralsPosition[i].Y, 30, 30);
                Rectangle MineralsRectangle2 = new Rectangle((int)MineralsPosition2[i].X, (int)MineralsPosition2[i].Y, 30, 30);
                if (personRectangle.Intersects(MineralsRectangle) == true)
                {
                    bool isPocketEmpty = false;
                    int pocketnumber = 0;

                    for (int j = 0; j < pocket.Length; j++)
                    {
                        if (pocket[j] == -1)
                        {
                            pocketnumber = j;
                            isPocketEmpty = true;
                            break;
                        }
                    }

                    if (isPocketEmpty)
                    {
                        pocket[pocketnumber] = MI[i];
                        playerhp += 0;
                        ManHit = true;
                        MineralsPosition[i].Y = r.Next(720, 1440);
                        MineralsPosition[i].X = r.Next(180, 1000);
                        MI[i] = r.Next(0, 4);

                        if (MediaPlayer.State == MediaState.Stopped && game.mCurrentScreen == game.mGameplayScreen)
                        {
                            effects[1].CreateInstance().Play();
                        }
                        else
                        {

                        }
                    }
                }
                if (personRectangle.Intersects(MineralsRectangle2) == true)
                {

                    int mineralIndex2 = MI[i];


                    switch (mineralIndex2)
                    {
                        case 0:
                            playerhp += 10;
                            playerenergy += 2;
                            break;
                        case 1:
                            playerenergy += 3;
                            break;
                        case 2:
                            playerhp -= 5;
                            playerenergy += 10;
                            break;
                        case 3:
                            playerenergy += 2;
                            break;
                    }

                    if (MediaPlayer.State == MediaState.Stopped && game.mCurrentScreen == game.mGameplayScreen)
                    {
                        effects[2].CreateInstance().Play();
                    }
                    else
                    {

                    }
                    
                    playerhp += 0;

                    MineralsPosition2[i].Y = r.Next(720, 1440);
                    MineralsPosition2[i].X = r.Next(180, 1000);
                    MI[i] = r.Next(0, 4);

                }
                else if (personRectangle.Intersects(MineralsRectangle) == false)
                {

                }
                else if (personRectangle.Intersects(MineralsRectangle2) == false)
                {

                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (personRectangle.Intersects(roomRectangle2) && Keyboard.GetState().IsKeyDown(Keys.R) == true && recharge == false)
                {
                    recharge = true;
                    playerenergy += 0.3f;
                    direction2 = 1;
                    Player_charge.UpdateFrame(elapsed);
                    effects[0].CreateInstance().Play();
                  
                }
                else
                {
                    
                    recharge = false;
                }
                if (playerenergy <= 20)
                {
                    if (MediaPlayer.State == MediaState.Stopped && game.mCurrentScreen == game.mGameplayScreen)
                    {
                        effects[3].CreateInstance().Play();
                    }

                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                if (Keyboard.GetState().IsKeyDown(Keys.A) == true)
                {
                    ScreenEvent.Invoke(game.mMainMenu, new EventArgs());
                    return;
                }
                else if (Keyboard.GetState().IsKeyDown(Keys.B) == true)
                {
                    ScreenEvent.Invoke(game.mGameplayScreen, new EventArgs());
                    return;
                }
            }


            if (personRectangle.Intersects(roomRectangle))
            {
                for (int i = 0; i < pocket.Length; i++)
                {
                    if (pocket[i] == -1) continue;

                    switch (pocket[i])
                    {
                        case 0:
                            score += 40;
                            Console.WriteLine("collect");
                            break;
                        case 1:
                            score += 10;
                            break;
                        case 2:
                            score += 20;
                            break;
                        case 3:
                            score += 30;
                            break;
                    }

                    pocket[i] = -1;

                }
                if (MediaPlayer.State == MediaState.Stopped && game.mCurrentScreen == game.mGameplayScreen)
                {
                    
                }
                else
                {

                }
            }

            base.Update(theTime);
        }

        public override void Draw(SpriteBatch theBatch)
        {
            //theBatch.Draw(gameplayTexture, Vector2.Zero, Color.White); base.Draw(theBatch);

            //BG
            theBatch.Draw(BG, (BGPos + new Vector2(0, 120) - cameraPos), Color.White);
            theBatch.Draw(BG, (BGPos - cameraPos) * scroll_factor + new Vector2(0, 720), Color.White);
            theBatch.Draw(BG, (BGPos - cameraPos) * scroll_factor + new Vector2(0, 1280), Color.White);
            //Sky 
            theBatch.Draw(Sky, (BGPos + new Vector2(0, 0) - cameraPos), new Rectangle(0, 0, 1280, 120), Color.White);
            theBatch.Draw(Sky, (BGPos + new Vector2(0, 0) - cameraPos), new Rectangle(0, 240, 1280, 120), new Color(1f, 1f, 1f, nightFactor));

            //Stair
            theBatch.Draw(Waterroom, (BGPos + new Vector2(600, 120) - cameraPos), new Rectangle(240, 0, 60, 180), Color.White);
            theBatch.Draw(Waterroom, (BGPos + new Vector2(600, 280) - cameraPos), new Rectangle(240, 0, 60, 180), Color.White);
            theBatch.Draw(Waterroom, (BGPos + new Vector2(600, 440) - cameraPos), new Rectangle(240, 0, 60, 180), Color.White);
            //Nature
            theBatch.Draw(Nature, (BGPos + new Vector2(0, 60) - cameraPos), new Rectangle(0, 0, 60, 60), Color.White);
            theBatch.Draw(Nature, (BGPos + new Vector2(60, 60) - cameraPos), new Rectangle(0, 0, 60, 60), Color.White);
            theBatch.Draw(Nature, (BGPos + new Vector2(120, 60) - cameraPos), new Rectangle(180, 0, 60, 60), Color.White);
            theBatch.Draw(Nature, (BGPos + new Vector2(180, 60) - cameraPos), new Rectangle(240, 0, 60, 60), Color.White);
            //wood
            theBatch.Draw(Nature, (BGPos + new Vector2(720, 0) - cameraPos), new Rectangle(0, 240, 60, 120), Color.White);
            theBatch.Draw(Nature, (BGPos + new Vector2(780, 0) - cameraPos), new Rectangle(0, 240, 60, 120), Color.White);
            theBatch.Draw(Nature, (BGPos + new Vector2(840, 0) - cameraPos), new Rectangle(0, 240, 60, 120), Color.White);
            theBatch.Draw(Nature, (BGPos + new Vector2(900, 0) - cameraPos), new Rectangle(0, 240, 60, 120), Color.White);
            theBatch.Draw(Nature, (BGPos + new Vector2(960, 0) - cameraPos), new Rectangle(0, 240, 60, 120), Color.White);
            theBatch.Draw(Nature, (BGPos + new Vector2(1020, 0) - cameraPos), new Rectangle(0, 240, 60, 120), Color.White);
            theBatch.Draw(Nature, (BGPos + new Vector2(1080, 0) - cameraPos), new Rectangle(0, 240, 60, 120), Color.White);


            //Bag- cameraPos
            theBatch.Draw(Bag, new Vector2(1160, 600), Color.White);
            theBatch.Draw(Box_1, new Vector2(1160, 480), Color.White);

            //room
            theBatch.Draw(Storage_BG, BGPos + new Vector2(185, 125) - cameraPos, Color.White);
            theBatch.Draw(Device_BG, BGPos + new Vector2(185, 305) - cameraPos, Color.Blue);
            theBatch.Draw(Charge_BG, BGPos + new Vector2(665, 125) - cameraPos, Color.White);
            theBatch.Draw(Sci_BG, BGPos + new Vector2(665, 305) - cameraPos, Color.Blue);

            Vector2 playercampos = BGPos + xPos - cameraPos;
            Rectangle personRectangle = new Rectangle((int)playercampos.X, (int)playercampos.Y, (int)size.X, (int)size.Y);
            theBatch.Draw(empty, personRectangle, Color.Red);

            //Score
            theBatch.DrawString(mineralScore, "Score : " + score, new Vector2(1000, 300), Color.White);

            //player
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (personRectangle.Intersects(roomRectangle2) && Keyboard.GetState().IsKeyDown(Keys.R) == false && recharge == false)
            {
                theBatch.DrawString(pressedPower, "Pressed R to charge power", new Vector2(730, 150), Color.Black);
            }
            if (personRectangle.Intersects(roomRectangle2) && Keyboard.GetState().IsKeyDown(Keys.R) == true)
            {
                Player_charge.DrawFrame(theBatch, xPos2, direction2);
                
          
            }
            else
                Player.DrawFrame(theBatch, playercampos + new Vector2(0, -40), direction);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //MInerals
            for (int i = 0; i < max_minerals; i++)
            {
                Vector2 minePos = BGPos + MineralsPosition[i] - cameraPos;
                theBatch.Draw(empty, new Rectangle((int)minePos.X, (int)minePos.Y, 30, 30), Color.Blue);
                theBatch.Draw(Minerals, minePos, new Rectangle(120 * MI[i] + 45, 0 + 45, 50, 50), Color.White);
                //theBatch.Draw(Minerals, minePos + new Vector2(-40,-40), new Rectangle(120 * MI[i],0 , 120, 120), Color.White);
            }
            //MInerals2
            for (int i = 0; i < max_minerals; i++)
            {
                Vector2 minePos2 = BGPos + MineralsPosition2[i] - cameraPos;
                theBatch.Draw(empty, new Rectangle((int)minePos2.X, (int)minePos2.Y, 30, 30), Color.Blue);
                theBatch.Draw(Minerals, minePos2, new Rectangle(120 * MI[i] + 45, 120 + 45, 50, 50), Color.White);

            }

            for (int i = 0; i < blockPosition.Count; i++)
            {
                for(int j = 0; j < blockPosition.Count; j++)
                {
                    Rectangle blockRect = new Rectangle((int)blockPosition[i].X, (int)blockPosition[i].Y, 50, 50);
                    blockRect.X = (int)(BGPos.X + blockRect.X - cameraPos.X);
                    blockRect.Y = (int)(BGPos.Y + blockRect.Y - cameraPos.Y);
                    theBatch.Draw(Sand, blockRect, Color.White);
                }
            }

            for (int i = 0; i < blockPosition2.Count; i++)
            {
                for (int j = 0; j < blockPosition2.Count; j++)
                {
                    Rectangle blockRect2 = new Rectangle((int)blockPosition2[i].X, (int)blockPosition2[i].Y, 50, 50);
                    blockRect2.X = (int)(BGPos.X + blockRect2.X - cameraPos.X);
                    blockRect2.Y = (int)(BGPos.Y + blockRect2.Y - cameraPos.Y);
                    theBatch.Draw(Sand, blockRect2, Color.White);
                }
            }

            for (int i = 0; i < blockPosition3.Count; i++)
            {
                for (int j = 0; j < blockPosition3.Count; j++)
                {
                    Rectangle blockRect3 = new Rectangle((int)blockPosition3[i].X, (int)blockPosition3[i].Y, 50, 50);
                    blockRect3.X = (int)(BGPos.X + blockRect3.X - cameraPos.X);
                    blockRect3.Y = (int)(BGPos.Y + blockRect3.Y - cameraPos.Y);
                    theBatch.Draw(Sand, blockRect3, Color.White);
                }
            }

            for (int i = 0; i < blockPosition4.Count; i++)
            {
                for (int j = 0; j < blockPosition4.Count; j++)
                {
                    Rectangle blockRect4 = new Rectangle((int)blockPosition4[i].X, (int)blockPosition4[i].Y, 50, 50);
                    blockRect4.X = (int)(BGPos.X + blockRect4.X - cameraPos.X);
                    blockRect4.Y = (int)(BGPos.Y + blockRect4.Y - cameraPos.Y);
                    theBatch.Draw(Sand, blockRect4, Color.White);
                }
            }

            for (int i = 0; i < blockPosition5.Count; i++)
            {
                for (int j = 0; j < blockPosition5.Count; j++)
                {
                    Rectangle blockRect5 = new Rectangle((int)blockPosition5[i].X, (int)blockPosition5[i].Y, 50, 50);
                    blockRect5.X = (int)(BGPos.X + blockRect5.X - cameraPos.X);
                    blockRect5.Y = (int)(BGPos.Y + blockRect5.Y - cameraPos.Y);
                    theBatch.Draw(Sand, blockRect5, Color.White);
                }
            }

            for (int i = 0; i < blockPosition6.Count; i++)
            {
                for (int j = 0; j < blockPosition6.Count; j++)
                {
                    Rectangle blockRect6 = new Rectangle((int)blockPosition6[i].X, (int)blockPosition6[i].Y, 50, 50);
                    blockRect6.X = (int)(BGPos.X + blockRect6.X - cameraPos.X);
                    blockRect6.Y = (int)(BGPos.Y + blockRect6.Y - cameraPos.Y);
                    theBatch.Draw(Sand, blockRect6, Color.White);
                }
            }

            for (int i = 0; i < blockPosition7.Count; i++)
            {
                for (int j = 0; j < blockPosition7.Count; j++)
                {
                    Rectangle blockRect7 = new Rectangle((int)blockPosition7[i].X, (int)blockPosition7[i].Y, 50, 50);
                    blockRect7.X = (int)(BGPos.X + blockRect7.X - cameraPos.X);
                    blockRect7.Y = (int)(BGPos.Y + blockRect7.Y - cameraPos.Y);
                    theBatch.Draw(Sand, blockRect7, Color.White);
                }
            }


            //minerals in bag
            theBatch.Draw(Minerals, new Vector2(1185, 490), new Rectangle(120 * pocket[0] + 45, 0 + 45, 50, 50), Color.White);
            theBatch.Draw(Minerals, new Vector2(1230, 490), new Rectangle(120 * pocket[1] + 45, 0 + 45, 50, 50), Color.White);
            theBatch.Draw(Minerals, new Vector2(1185, 540), new Rectangle(120 * pocket[2] + 45, 0 + 45, 50, 50), Color.White);
            theBatch.Draw(Minerals, new Vector2(1230, 540), new Rectangle(120 * pocket[3] + 45, 0 + 45, 50, 50), Color.White);

            //BAR
            theBatch.Draw(BAR, new Vector2(30, 600), new Rectangle(0, 0, 320, 110), Color.White);
            theBatch.Draw(BAR, new Vector2(350, 600), new Rectangle(0, 100, 320, 110), Color.White);

            theBatch.Draw(empty, new Vector2(75, 640), new Rectangle(0, 0, (int) playerhp, 20), Color.Red);
            theBatch.Draw(empty, new Vector2(395, 635), new Rectangle(0, 0, (int) playerenergy, 20), Color.Yellow);

        }

        protected void processinput()
        {
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Up) == true)
            {
                direction = 6;
                if(xPos.Y < 0)
                {
                    xPos.Y = 0;
                }
                if (xPos.Y < cameraPos.Y)
                {
                    cameraPos -= new Vector2(0, 720);

                }
                xPos.Y -= 3;
                playerenergy -= 1 * elapsed;
            }

            if (ks.IsKeyDown(Keys.Left) == true)
            {
                direction = 5;
                if(xPos.X < 0)
                {
                    xPos.X = 0;
                }
                xPos.X -= 3;
                playerenergy -= 1 * elapsed;
            }

            if (ks.IsKeyDown(Keys.Down) == true)
            {
                direction = 3;
                if(xPos.Y > 2080)
                {
                    xPos.Y = 2080;
                }
                if (xPos.Y > cameraPos.Y + 640)
                {
                    cameraPos += new Vector2(0, 720);
                }
                xPos.Y += 3;
                playerenergy -= 1 * elapsed;
            }

            if (ks.IsKeyDown(Keys.Right) == true)
            {
                direction = 4;
                if(xPos.X > 1220)
                {
                    xPos.X = 1220;
                }
                xPos.X += 3;
                playerenergy -= 1 * elapsed;
            }

            if (ks.IsKeyDown(Keys.Space) == true && drill == false)
            {
                drill = true;
                playerenergy -= 3;

                Rectangle hitbox = new Rectangle((int)(xPos.X + size.X / 2 -15), (int)(xPos.Y + size.Y / 2 -15), 30, 30);

                switch (direction)
                {
                    case 3:
                    case 7:
                        hitbox.Y += (int)size.Y / 2;
                        break;
                    case 4:
                    case 1:
                        hitbox.X += (int)size.X / 2;
                        break;
                    case 5:
                    case 2:
                        hitbox.X -= (int)size.X / 2;
                        break;
                    case 6:
                    case 8:
                        hitbox.Y -= (int)size.Y / 2;
                        break;
                }
                Console.WriteLine(hitbox);
                for (int i = 0; i < blockPosition.Count; i++)
                {
                    Rectangle blockRect = new Rectangle((int)blockPosition[i].X, (int)blockPosition[i].Y, 50, 50);
                    if (hitbox.Intersects(blockRect))
                    {
                        Console.WriteLine("hit");
                        blockPosition.Remove(blockPosition[i]);
                    }
                }

                for (int i = 0; i < blockPosition2.Count; i++)
                {
                    Rectangle blockRect2 = new Rectangle((int)blockPosition2[i].X, (int)blockPosition2[i].Y, 50, 50);
                    if (hitbox.Intersects(blockRect2))
                    {
                        Console.WriteLine("hit");
                        blockPosition2.Remove(blockPosition2[i]);
                    }
                }

                for (int i = 0; i < blockPosition3.Count; i++)
                {
                    Rectangle blockRect3 = new Rectangle((int)blockPosition3[i].X, (int)blockPosition3[i].Y, 50, 50);
                    if (hitbox.Intersects(blockRect3))
                    {
                        Console.WriteLine("hit");
                        blockPosition3.Remove(blockPosition3[i]);
                    }
                }

                for (int i = 0; i < blockPosition4.Count; i++)
                {
                    Rectangle blockRect4 = new Rectangle((int)blockPosition4[i].X, (int)blockPosition4[i].Y, 50, 50);
                    if (hitbox.Intersects(blockRect4))
                    {
                        Console.WriteLine("hit");
                        blockPosition4.Remove(blockPosition4[i]);
                    }
                }

                for (int i = 0; i < blockPosition5.Count; i++)
                {
                    Rectangle blockRect5 = new Rectangle((int)blockPosition5[i].X, (int)blockPosition5[i].Y, 50, 50);
                    if (hitbox.Intersects(blockRect5))
                    {
                        Console.WriteLine("hit");
                        blockPosition5.Remove(blockPosition5[i]);
                    }
                }

                for (int i = 0; i < blockPosition6.Count; i++)
                {
                    Rectangle blockRect6 = new Rectangle((int)blockPosition6[i].X, (int)blockPosition6[i].Y, 50, 50);
                    if (hitbox.Intersects(blockRect6))
                    {
                        Console.WriteLine("hit");
                        blockPosition6.Remove(blockPosition6[i]);
                    }
                }

                for (int i = 0; i < blockPosition7.Count; i++)
                {
                    Rectangle blockRect7 = new Rectangle((int)blockPosition7[i].X, (int)blockPosition7[i].Y, 50, 50);
                    if (hitbox.Intersects(blockRect7))
                    {
                        Console.WriteLine("hit");
                        blockPosition7.Remove(blockPosition7[i]);
                    }
                }

            }
            if (ks.IsKeyUp(Keys.Space))
            {
                drill = false;
            }

            if (ks.IsKeyUp(Keys.Right) && ks.IsKeyUp(Keys.Left) && ks.IsKeyUp(Keys.Up) && ks.IsKeyUp(Keys.Down))
            {
                switch (direction)
                {
                    case 3:
                        direction = 7;
                        break;
                    case 4:
                        direction = 1;
                        break;
                    case 5:
                        direction = 2;
                        break;
                    case 6:
                        direction = 8;
                        break;
                }
            }

        }
    }
}