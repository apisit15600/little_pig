﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Screen;

namespace GameProject
{
    public class GameOver : screen
    {
        //Texture2D gameoverTexture;
        Texture2D menuTexture;
        Texture2D menuTexture2;
        Texture2D gameoverbg;
        Song gameoversong;
        Game1 game;

        int currentMenu = 1;
        bool keyActiveUp = false;
        bool keyActiveDown = false;
        public GameOver(Game1 game, EventHandler theScreenEvent)
        : base(theScreenEvent)
        {

            //gameoverTexture = game.Content.Load<Texture2D>("Menu");
            gameoverbg = game.Content.Load<Texture2D>("Endgame");
            gameoversong = game.Content.Load<Song>("GAME_OVER");
            menuTexture = game.Content.Load<Texture2D>("Menu");
            menuTexture2 = game.Content.Load<Texture2D>("menu_sprite");
           
            //Load the background texture for the screen
            //menuTexture = game.Content.Load<Texture2D>("title");
            this.game = game;
        }
        public override void Update(GameTime theTime)
        {
            KeyboardState keyboard = Keyboard.GetState();
            if (keyboard.IsKeyDown(Keys.Up))
            {
                if (keyActiveUp == true)
                {
                    if (currentMenu > 1)
                    {
                        currentMenu = currentMenu - 1;
                        keyActiveUp = false;
                    }
                }
            }

            if (keyboard.IsKeyDown(Keys.Down))
            {
                if (keyActiveDown == true)
                {
                    if (currentMenu < 2)
                    {
                        currentMenu = currentMenu + 1;
                        keyActiveDown = false;
                    }
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) == true && currentMenu == 1)
            {
                ScreenEvent.Invoke(game.mMainMenu, new EventArgs());
                return;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) == true && currentMenu == 2)
            {
                game.Exit();
                return;
            }
            if (MediaPlayer.State == MediaState.Stopped && game.mCurrentScreen == game.mgameoverscreen)
            {
                MediaPlayer.Play(gameoversong);

            }
            else
            {

            }

            base.Update(theTime);
        }
        public override void Draw(SpriteBatch theBatch)
        {
            theBatch.Draw(gameoverbg, Vector2.Zero, Color.White); base.Draw(theBatch);
            if(currentMenu == 1)
            {
                theBatch.Draw(menuTexture2, new Vector2(300, 575), new Rectangle(0, 0, 240, 120), Color.White);
            }
            else
            {

                theBatch.Draw(menuTexture2, new Vector2(300, 575), new Rectangle(240,0, 240, 120), Color.White);
            }
            if (currentMenu == 2)
            {

                theBatch.Draw(menuTexture, new Vector2(600, 575), new Rectangle(0, 120, 240, 120), Color.White);
            }
            else
            {

                theBatch.Draw(menuTexture, new Vector2(600, 575), new Rectangle(240, 120, 240, 120), Color.White);
            }


        }   
    }
}