﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Screen;

namespace GameProject
{
    public class Cutscene : screen
    {
        Video vid_cutscene;
        VideoPlayer cutPlayer;
        Texture2D vidTexture;
        Rectangle vidRectangle;
        SpriteFont start;
        Game1 game;

        public Cutscene(Game1 game, EventHandler theScreenEvent)
        : base(theScreenEvent)
        {
            cutPlayer = new VideoPlayer();
            start = game.Content.Load<SpriteFont>("Start");
            vid_cutscene = game.Content.Load<Video>("vid_cutscene");
            cutPlayer.Play(vid_cutscene);
            vidRectangle = new Rectangle(game.GraphicsDevice.Viewport.X, game.GraphicsDevice.Viewport.Y, game.GraphicsDevice.Viewport.Width, game.GraphicsDevice.Viewport.Height);

            //Load the background texture for the screen
            //menuTexture = game.Content.Load<Texture2D>("title");
            this.game = game;
        }
        public override void Update(GameTime theTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.P) && game.mCurrentScreen == game.mCutscene == true)
            {
                ScreenEvent.Invoke(game.mGameplayScreen, new EventArgs());
                return;
            }

            if (MediaPlayer.State == MediaState.Playing && game.mCurrentScreen == game.mCutscene)
            {
                MediaPlayer.Stop();
            }
            else
            {

            }

            if (cutPlayer.State == MediaState.Stopped)
            {

                cutPlayer.Play(vid_cutscene);

            }





            base.Update(theTime);
        }
        public override void Draw(SpriteBatch theBatch)
        {

            vidTexture = cutPlayer.GetTexture();
            theBatch.Draw(vidTexture, vidRectangle, Color.White);

            if (cutPlayer.State == MediaState.Playing)
            {
                theBatch.DrawString(start, "Pressed P to Start game", new Vector2(600, 670), Color.White);

            }
        }
    }
}