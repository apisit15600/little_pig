﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Screen;

namespace GameProject
{
    public class MainMenu : screen
    {
        Texture2D menuTexture;
        Texture2D logo;
        Game1 game;

        //Song
        Song song;
        //Video
        Video vid;
        VideoPlayer vidPlayer = new VideoPlayer();
        Texture2D vidTexture;
        Rectangle vidRectangle;

        int currentMenu = 1;
        bool keyActiveUp = false;
        bool keyActiveDown = false;

        public MainMenu(Game1 game, EventHandler theScreenEvent)
        : base(theScreenEvent)
        {
            this.song = game.Content.Load<Song>("MENU SOUND");
            menuTexture = game.Content.Load<Texture2D>("Menu");
            logo = game.Content.Load<Texture2D>("logo");
            vid = game.Content.Load<Video>("BG_VID");
            vidPlayer.Play(vid);
            vidRectangle = new Rectangle(game.GraphicsDevice.Viewport.X, game.GraphicsDevice.Viewport.Y, game.GraphicsDevice.Viewport.Width, game.GraphicsDevice.Viewport.Height);

            //Load the background texture for the screen
            //menuTexture = game.Content.Load<Texture2D>("title");
            this.game = game;
        }
        void MediaPlayer_MediaStateChanged(object sender, System.EventArgs e)
        {
            // 0.0f is silent, 1.0f is full volume
            MediaPlayer.Volume -= 0.1f;
            MediaPlayer.Play(song);

        }
        public override void Update(GameTime theTime)
        {
            KeyboardState keyboard = Keyboard.GetState();
            if (keyboard.IsKeyDown(Keys.Up))
            {
                if (keyActiveUp == true)
                {
                    if (currentMenu > 1)
                    {
                        currentMenu = currentMenu - 1;
                        keyActiveUp = false;
                    }
                }
            }
            
            if (keyboard.IsKeyDown(Keys.Down))
            {
                if (keyActiveDown == true)
                {
                    if (currentMenu < 3)
                    {
                        currentMenu = currentMenu + 1;
                        keyActiveDown = false;
                    }
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) == true && currentMenu == 1)
            {
                game.mGameplayScreen.reset();
                ScreenEvent.Invoke(game.mCutscene, new EventArgs());
                return;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) == true && currentMenu == 2)
            {
                ScreenEvent.Invoke(game.mhowState, new EventArgs());
                return;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) == true && currentMenu == 3)
            {
                game.Exit();
            }

            // check keyup
            if (keyboard.IsKeyUp(Keys.Up))
            {
                keyActiveUp = true;
            }
            if (keyboard.IsKeyUp(Keys.Down))
            {
                keyActiveDown = true;
            }

            if (MediaPlayer.State != MediaState.Playing)
            {
                MediaPlayer.Play(song);
            }
            else
            {

            }

           
            base.Update(theTime);
        }
        public override void Draw(SpriteBatch theBatch)
        {
            vidTexture = vidPlayer.GetTexture();
            theBatch.Draw(vidTexture, vidRectangle, Color.White);
            theBatch.Draw(logo, new Vector2(300, 70), Color.White);

            if (currentMenu == 1)
            {
                theBatch.Draw(menuTexture, new Vector2(450, 350), new Rectangle(0, 0, 240, 120), Color.White);
            }
            else
            {
                theBatch.Draw(menuTexture, new Vector2(450, 350), new Rectangle(240, 0, 240, 120), Color.White);
            }

            if (currentMenu == 2)
            {
                theBatch.Draw(menuTexture, new Vector2(450, 460), new Rectangle(0, 240, 240, 120), Color.White);
            }
            else
            {

                theBatch.Draw(menuTexture, new Vector2(450, 460), new Rectangle(240, 240, 240, 120), Color.White);
            }
            if (currentMenu == 3)
            {

                theBatch.Draw(menuTexture, new Vector2(450, 575), new Rectangle(0, 120, 240, 120), Color.White);
            }
            else
            {

                theBatch.Draw(menuTexture, new Vector2(450, 575), new Rectangle(240, 120, 240, 120), Color.White);
            }
        }
    }
}