﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Screen;

namespace GameProject
{
    public class HowState : screen
    {
        Game1 game;

        public HowState(Game1 game, EventHandler theScreenEvent)
        : base(theScreenEvent)
        {


            //Load the background texture for the screen
            //menuTexture = game.Content.Load<Texture2D>("title");
            this.game = game;
        }
        public override void Update(GameTime theTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                ScreenEvent.Invoke(game.mMainMenu, new EventArgs());
                return;
            }
            base.Update(theTime);
        }
        public override void Draw(SpriteBatch theBatch)
        {
            game.GraphicsDevice.Clear(Color.CornflowerBlue);


        }
    }
}